import React, { useState } from 'react';
import './App.css';
import { Button, Paper, styled } from '@mui/material';
import axios from 'axios';


const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  textAlign: 'center',
  color: theme.palette.text.secondary,
  height: 60,
  lineHeight: '60px',
}));

function App() {


  const [error, setError] = useState<string | null>(null);
  const [text, setText] = useState<string | null>(null);
  const get = () => {
    axios<string>(`${process.env.REACT_APP_BACK_URL}/WeatherForecast`)
      .then(res => {
        setError(null);
        var myObject = JSON.stringify(res.data);
        setText(myObject);
      })
      .catch(error => setError(JSON.stringify(error)));
  };


  return (
    <div className="App">
      <Button onClick={get} variant='contained'>Клик GET</Button>
      {text && <Paper className="p-10" elevation={2}>
        {text}
      </Paper>}
      {error && <Paper className="red p-10" elevation={2} >
        {error}
      </Paper >}
    </div>


  );
}

export default App;
